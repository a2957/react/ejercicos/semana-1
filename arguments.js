

function addTogether() {
        
        if (arguments.length === 2) {
            if (typeof arguments[0] === 'number' && typeof arguments[1] === 'number') {
                return arguments[0] + arguments[1];
            } else {
                return undefined
            }
            
        }
        else if (arguments.length === 1) {
            if (typeof arguments[0] === 'number'){
                return (x) => arguments[0] + x;
            } else {
                return undefined
            }
        }
}


const result = addTogether(2)(3);
// const result = sumTwoAnd(3)

console.log(result);

