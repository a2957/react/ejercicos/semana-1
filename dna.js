

function pairElement(str) {
    let array = str.split('');
    let newArray = [];
    array.forEach(l => {
        if (l === 'C') {
            newArray.push([l,'G']);
        } else if (l === 'G') {
            newArray.push([l,'C']);
        } else if (l === 'A') {
            newArray.push([l,'T']);
        } else if (l === 'T') {
            newArray.push([l,'A']);
        }
    })
    return newArray
}


cadena = 'CTCTA';

const result = pairElement(cadena)
console.log(result);