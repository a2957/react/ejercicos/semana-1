function truthCheck(collection, pre) {
  let truthy = 0;
  collection.forEach((element) => {
    if (element[pre]) {
      truthy++;
    }
  });

  return truthy === collection.length ? true : false;

}

const result = truthCheck(
  [
    { user: "Layla", sex: "male" },
    { user: "Kappa", sex: "male" },
    { user: "Himiko", sex: "female" },
    { user: "Sassy", sex: "female" },
  ],
  "sex"
);

console.log(result);
